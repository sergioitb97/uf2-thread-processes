package second_exercise;

//Crea una classe de Prova que crida a 3 threads per esbrinar quins són els següents primers.
public class ex2RunnableTesting {
    public static void main(String[] args) {

        ex2Runnable r1 = new ex2Runnable(53434);
        ex2Runnable r2 = new ex2Runnable(32233);
        ex2Runnable r3 = new ex2Runnable(22);


        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);


        t1.start();
        t2.start();
        t3.start();

    }
}
