package second_exercise;

//Feu el mateix exercici però fent que la classe que calcula el següent primer d’un long implementi la interface Runnable
public class ex2Runnable implements Runnable {

    private final long givenLong;

    public ex2Runnable(long givenLong) {
        this.givenLong = givenLong;
    }

    // escriu una funció que rebi un long i retorni true si aquell long és primer (no té divisors més que ell i l’1)
    public static boolean isPrime(long givenLong){

        if(givenLong < 2) return false;
        if(givenLong == 2 || givenLong == 3) return true;
        if(givenLong%2 == 0 || givenLong%3 == 0) return false;
        long sqrtN = (long)Math.sqrt(givenLong)+1;
        for(long i = 6L; i <= sqrtN; i += 6) {
            if(givenLong%(i-1) == 0 || givenLong%(i+1) == 0) return false;
        }
        return true;
    }

    //Funcio que calculi i mostri quin és el següent primer més proper a un long donat.
    public static void nextPrimeValue(long longGivenValue){

        boolean primeFound = false;
        long nextPrime = 0;

        while (!primeFound){
            longGivenValue++;
            if (isPrime(longGivenValue)){
                primeFound = true;
                nextPrime = longGivenValue;
            }
        }
        System.out.println("The thread: " + Thread.currentThread().getName() + " has found the next prime number: " + nextPrime);
    }

    @Override
    public void run() {
        nextPrimeValue(givenLong);
    }

}
