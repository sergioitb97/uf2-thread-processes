package basic_first_test.runnable;

import java.time.Duration;
import java.time.LocalTime;

public class RunnableBike {

    private String nom;

    private LocalTime inici;

    private int distancia;

    private long temps;

    public RunnableBike(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public LocalTime getInici() {
        return inici;
    }

    public void setTemps(long temps) {
        this.temps = temps;
    }

    public String getNom() {
        return nom;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public String toString() {
        return "versio1.Bici{" +
                "nom='" + nom + '\'' +
                ", inici=" + inici +
                ", distancia=" + distancia +
                ", temps=" + temps +
                '}';
    }
}
