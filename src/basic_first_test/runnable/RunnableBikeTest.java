package basic_first_test.runnable;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class RunnableBikeTest extends RunnableBike implements Runnable{



    public RunnableBikeTest(String nom, int distancia, LocalTime inici) {
        super(nom, distancia, inici);
    }

    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        RunnableBikeTest b1 = new RunnableBikeTest("Jorge", DISTANCIA, inici);
        RunnableBikeTest b2 = new RunnableBikeTest("Paco", DISTANCIA, inici);
        RunnableBikeTest b3 = new RunnableBikeTest("Ana", DISTANCIA, inici);
        RunnableBikeTest b4 = new RunnableBikeTest("Matias", DISTANCIA, inici);
        RunnableBikeTest b5 = new RunnableBikeTest("Joe", DISTANCIA, inici);

        //crear los threads con los objetos bici
        Thread t1 = new Thread(b1);
        Thread t2 = new Thread(b2);
        Thread t3 = new Thread(b3);
        Thread t4 = new Thread(b4);
        Thread t5 = new Thread(b5);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String winnerName = b1.getNom();
        long recordTime = b1.getTemps();

        if (b2.getTemps() < recordTime) {
            System.out.println("1");
            recordTime = b2.getTemps();
            winnerName = b2.getNom();
        }
        if (b3.getTemps() < recordTime) {
            System.out.println("3");
            winnerName = b3.getNom();
            recordTime = b3.getTemps();
        }
        if (b4.getTemps() < recordTime) {
            System.out.println("4");
            winnerName = b4.getNom();
            recordTime = b4.getTemps();
        }
        if (b5.getTemps() < recordTime) {
            System.out.println("5");
            winnerName = b5.getNom();
            recordTime = b5.getTemps();
        }
        System.out.println("La bici més ràpida ha sigut la de " + winnerName + " que ha trigat " + recordTime + " unitats de temps.");



    }


    @Override
    public void run() {
            System.out.println("\nBicicleta del " + getNom() + " està en marxa dins el thread "+ Thread.currentThread().getName());



            for(int i=0;i<1000;i++){
                //System.out.println("Distancia bici " + nom +" = "+ i);
                if (i == 500){
                    Thread.currentThread().interrupt();
                    if(Thread.currentThread().isInterrupted()){
                        System.out.println("A LA MIERDA LOCO");
                    }
                }
            }
            LocalTime horaFinal = LocalTime.now();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long aux= Duration.between(getInici(),horaFinal).getNano();
            setTemps(aux);

            System.out.println("Temps transcorregut de " + getNom() + " : " +getTemps());


    }
}
