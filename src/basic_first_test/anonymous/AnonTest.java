package basic_first_test.anonymous;

import basic_first_test.object.Bike;

import java.time.Duration;
import java.time.LocalTime;

public class AnonTest {

    public static void main(String[] args) {

        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        Bike b1 = new Bike("Jorge", DISTANCIA, inici);
        Bike b2 = new Bike("Paco", DISTANCIA, inici);
        Bike b3 = new Bike("Ana", DISTANCIA, inici);

        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i<DISTANCIA; i++){

                }
                LocalTime horaFinal = LocalTime.now();
                long aux= Duration.between(inici,horaFinal).getNano();
                b1.setTemps(aux);
            }
        });

        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i<DISTANCIA; i++){}
                LocalTime horaFinal = LocalTime.now();
                long aux= Duration.between(inici,horaFinal).getNano();
                b2.setTemps(aux);
            }
        });

        Thread t3=new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i<DISTANCIA; i++){}
                LocalTime horaFinal = LocalTime.now();
                long aux= Duration.between(inici,horaFinal).getNano();
                b3.setTemps(aux);
            }
        });




        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
        }


        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        String winnerName;
        long recordTime;

        if (b1.getTemps() < b2.getTemps()) {
            if (b1.getTemps() < b3.getTemps()) {
                winnerName = b1.getNom();
                recordTime = b1.getTemps();
            } else {
                winnerName = b3.getNom();
                recordTime = b3.getTemps();
            }

        } else {
            if (b2.getTemps() < b3.getTemps()) {
                winnerName = b2.getNom();
                recordTime = b2.getTemps();
            } else {
                winnerName = b3.getNom();
                recordTime = b3.getTemps();
            }
        }
        System.out.println("La bici més ràpida ha sigut la de " + winnerName + " que ha trigat " + recordTime + " unitats de recordTime.");

    }
}