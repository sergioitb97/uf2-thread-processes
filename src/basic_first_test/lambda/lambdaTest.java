package basic_first_test.lambda;

import basic_first_test.object.Bike;

import java.time.Duration;
import java.time.LocalTime;

public class lambdaTest {

    static LocalTime inici = LocalTime.now();
    final static int DISTANCIA = 1000;

    public static void main(String[] args) {


        Bike b1 = new Bike("Jorge", DISTANCIA, inici);
        Bike b2 = new Bike("Paco", DISTANCIA, inici);
        Bike b3 = new Bike("Ana", DISTANCIA, inici);

        Thread t1 = new Thread(() -> auxRunMethod(b1));
        Thread t2 = new Thread(() -> auxRunMethod(b2));
        Thread t3 = new Thread(() -> auxRunMethod(b3));

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
        }

        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        String winnerName = b1.getNom();
        long recordTime = b1.getTemps();
        if (b2.getTemps() < recordTime) {
            recordTime = b2.getTemps();
            winnerName = b2.getNom();
        }
        if (b3.getTemps() < recordTime) {
            winnerName = b3.getNom();
        }
        System.out.println("La bici més ràpida ha sigut la de " + winnerName + " que ha trigat " + recordTime + " unitats de temps.");
    }

    public static void auxRunMethod(Bike b) {
        for (int i = 0; i < DISTANCIA; i++) {

        }
        LocalTime acabat = LocalTime.now();
        long aux = Duration.between(inici, acabat).getNano();
        b.setTemps(aux);

    }


}
