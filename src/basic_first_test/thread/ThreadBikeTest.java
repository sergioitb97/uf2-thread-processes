package basic_first_test.thread;

import java.time.LocalTime;

public class ThreadBikeTest {
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        ThreadBike b1 = new ThreadBike("Jorge", DISTANCIA, inici);
        ThreadBike b2 = new ThreadBike("Paco", DISTANCIA, inici);
        ThreadBike b3 = new ThreadBike("Ana", DISTANCIA, inici);



        //Starting the threads
        b1.start();
        b2.start();
        b3.start();

        try {
                //Join allows different threads to run according to concurrency, if we don't use it we won't be able to get the
                //data that we want to measure since the process will run without giving time to the thread to take the data
            b1.join();
            b2.join();
            b3.join();
        } catch (InterruptedException e) {
        }

        /* We can call the run method from Thread but this will break the concurrency of the threads
        b1.run();
        b2.run();
        b3.run();*/

        //We can print the to string content also
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        String nom;
        long temps;
        if (b1.getTemps() < b2.getTemps()) {
            if (b1.getTemps() < b3.getTemps()) {
                nom = b1.getNom();
                temps = b1.getTemps();
            } else {
                nom = b3.getNom();
                temps = b3.getTemps();
            }
        } else {
            if (b2.getTemps() < b3.getTemps()) {
                nom = b2.getNom();
                temps = b2.getTemps();
            } else {
                nom = b3.getNom();
                temps = b3.getTemps();
            }
        }
        System.out.println("La bici més ràpida ha sigut la de " + nom + " que ha trigat " + temps + " unitats de temps.");
    }

}
