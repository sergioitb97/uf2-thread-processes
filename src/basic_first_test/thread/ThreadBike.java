package basic_first_test.thread;

import java.time.Duration;
import java.time.LocalTime;

public class ThreadBike extends Thread {

    private final String nom;//nom del propietari de la bici

    private final LocalTime inici;//moment en què comença el viatge en bici

    private final int distancia;// distància a recórrer

    private long temps;//temps transcorregut entre inici i completar la distància

    public ThreadBike(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }


    public String getNom() {
        return nom;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public void run() {
        for (int i = 0; i < distancia; i++) {
            //chivato System.out.println("Marca de " + nom +" = "+ i);
        }
        LocalTime horaFinal = LocalTime.now();

        long aux = Duration.between(inici, horaFinal).getNano();
        temps = aux;

    }


    @Override
    public String toString() {
        return "Thread: " + getName() +
                "{" + nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia + "}";
    }
}
