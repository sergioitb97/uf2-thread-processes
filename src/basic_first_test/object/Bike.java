package basic_first_test.object;

import java.time.LocalTime;

public class Bike{
    //nom del propietari de la bici
    private String nom;
    //moment en què comença el viatge en bici
    private LocalTime inici;
    // distància a recórrer
    private int distancia;
    //temps transcorregut entre inici i completar la distància
    private long temps;

    public Bike(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public String toString() {
        return nom+" ha trigat "+temps+" unitats de temps en fer una distancia de "+distancia+"\n";
    }

    public long getTemps() {
        return temps;
    }

    public void setTemps(long temps) {
        this.temps = temps;
    }
}