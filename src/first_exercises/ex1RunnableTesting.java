package first_exercises;

public class ex1RunnableTesting {

    public static void main(String[] args) {

        ex1Runnable r1 = new ex1Runnable("src/resources/ex1.1.txt");
        ex1Runnable r2 = new ex1Runnable("src/resources/ex1.2.txt");
        ex1Runnable r3 = new ex1Runnable("src/resources/ex1.3.txt");
        ex1Runnable r4 = new ex1Runnable("src/resources/ex1.4.txt");

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        Thread t4 = new Thread(r4);

        t1.start();
        t2.start();
        t3.start();
        t4.start();


    }
}
