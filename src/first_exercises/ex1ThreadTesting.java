package first_exercises;

import java.io.File;

public class ex1ThreadTesting {
    //Crea una classe de Prova que crida a 3 threads per esbrinar quantes línies té cada fitxer. Proveu amb fitxers grans i petits.
    public static void main(String[] args) {


        ex1Thread t1 = new ex1Thread("src/resources/ex1.1.txt");
        ex1Thread t2 = new ex1Thread("src/resources/ex1.2.txt");
        ex1Thread t3 = new ex1Thread("src/resources/ex1.3.txt");
        ex1Thread t4 = new ex1Thread("src/resources/ex1.4.txt");


        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }
}
