package first_exercises;

import java.io.*;

public class ex1Thread extends Thread {

    // El nom del fitxer serà un atribut del Thread de tipus String.
    private String givenStrFile;

    public ex1Thread(String givenStrFile) {
        this.givenStrFile = givenStrFile;
    }

    //escriu una funció que obri un fitxer de text i compti quantes línies té.
    public static void countFile(String givenStrFile) {

        File givenFile = new File(givenStrFile);
        try {
            FileReader fr = new FileReader((givenFile));
            BufferedReader fich = new BufferedReader(fr);
            //Usamos la clase BufferReadeader para tener acceso a un metodo propio (readLine()) y asi mediante un contador contar las lineas.
            int contadorL = 0;
            String linea;
            try {
                //En este caso la condicion final del while corresponde a null, para indicar el final de linea
                while ((linea = fich.readLine()) != null) {
                    if (!linea.isEmpty()) {
                        contadorL++;
                    }
                }
                System.out.println("The thread: " + Thread.currentThread().getName() + " has read in the given file " + givenFile.getName().toString() + " " + contadorL + " lines.");
                fich.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    //crea un Thread que dintre del seu mètode run() compti i mostri quantes línies té un fitxer (reutilitza el codi anterior).
    @Override
    public void run() {
        super.run();
        countFile(givenStrFile);
    }
}
